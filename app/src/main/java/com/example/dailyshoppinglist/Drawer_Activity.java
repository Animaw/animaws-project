package com.example.dailyshoppinglist;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.dailyshoppinglist.saving_and_fun.About_us_Activity;
import com.example.dailyshoppinglist.saving_and_fun.Bios_Activity;
import com.example.dailyshoppinglist.saving_and_fun.Help_Activity;
import com.example.dailyshoppinglist.saving_and_fun.Joke_Activity;
import com.example.dailyshoppinglist.saving_and_fun.Proverb_Activity;
import com.example.dailyshoppinglist.saving_and_fun.Saving_Activity;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.navigation.NavigationView;

public class Drawer_Activity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
   private DrawerLayout drawerLayout;
   private Toolbar toolbar;
   private NavigationView navigationView;
   private ActionBarDrawerToggle toggle;
   private Button log_in;
   private AdView adView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer_);

        drawerLayout = findViewById(R.id.drawer);
        toolbar = findViewById(R.id.darawer_toolBar);
        navigationView = findViewById(R.id.navigation_view);
        log_in = findViewById(R.id.drawer_button);
        adView = findViewById(R.id.adView);

     // initializing Mobile ads
        MobileAds.initialize(this, "ca-app-pub-8895797605041335~6791662848");
        AdRequest adRequest = new  AdRequest.Builder().build();
        adView.loadAd(adRequest);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,R.string.drawerOpen, R.string.drawerClosed);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        log_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

switch (menuItem.getItemId()){
    case  R.id.saving_concept:
        startActivity(new Intent(getApplicationContext(), Saving_Activity.class));
        break;
    case  R.id.proverbs:
        startActivity(new Intent(getApplicationContext(), Proverb_Activity.class));
        break;
    case  R.id.jokes:
        startActivity(new Intent(getApplicationContext(), Joke_Activity.class));
        break;
    case  R.id.bios:
        startActivity(new Intent(getApplicationContext(), Bios_Activity.class));
        break;
    case  R.id.about_us:
        startActivity(new Intent(getApplicationContext(), About_us_Activity.class));
        break;
    case  R.id.help:
        startActivity(new Intent(getApplicationContext(), Help_Activity.class));
        break;
    case  R.id.share_it:
        Intent shareIntent =   new Intent(android.content.Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT,"Insert Subject here");
        startActivity(Intent.createChooser(shareIntent, "Share via"));
        break;
}
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.exit_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.exit:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
