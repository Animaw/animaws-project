package com.example.dailyshoppinglist;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;

public class MainActivity extends AppCompatActivity {
    private EditText email, password;
    private TextView signup;
    private Button button_login;
    private FirebaseAuth myAuth;
    private ProgressDialog myproProgressDialog;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        email = findViewById(R.id.email_logon);
        password = findViewById(R.id.password_logon);
        signup = findViewById(R.id.signup_text);
        button_login = findViewById(R.id.btn_login);

        toolbar = findViewById(R.id.log_in_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("log in page");

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        myproProgressDialog = new ProgressDialog(this);
        myAuth = FirebaseAuth.getInstance();
        if (myAuth.getCurrentUser() != null){
            startActivity(new Intent(getApplicationContext(),HomeActivity.class));
        }


        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String myEmail = email.getText().toString().trim();
                String myPassword = password.getText().toString().trim();

                if (TextUtils.isEmpty(myEmail)){
                    email.setError("required field..");
                    email.requestFocus();
                    return;
                }
                if (!android.util.Patterns.EMAIL_ADDRESS.matcher(myEmail).matches()) {
                    email.setError("Enter a valid email");
                    email.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(myPassword)){
                    password.setError("required field..");
                    password.requestFocus();
                    return;
                }
                myproProgressDialog.setMessage("Processing...");
                myproProgressDialog.show();
                myAuth.signInWithEmailAndPassword(myEmail, myPassword).addOnCompleteListener(
                        new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                        startActivity(new Intent(getApplicationContext(),HomeActivity.class));
                        Toast.makeText(getApplicationContext(), "successfull", Toast.LENGTH_LONG).show();
                        myproProgressDialog.dismiss();

                    }
                    else{
                        if(task.getException() instanceof FirebaseAuthUserCollisionException){
                            Toast.makeText(getApplicationContext(), "email is already taken", Toast.LENGTH_LONG).show();
                            myproProgressDialog.dismiss();
                        }else{
                            Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_LONG).show();
                            myproProgressDialog.dismiss();
                        }

                    }
                    }
                });

            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),RegistrationActivity.class));

            }
        });
    }
}
