package com.example.dailyshoppinglist.Model;

public class Data {

    private String type;
    private String description;
    private String id;
    private int amount;
    private String date;

    public Data() {

    }

    public Data(String type, String description, String id, int amount, String date) {
        this.type = type;
        this.description = description;
        this.id = id;
        this.amount = amount;
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
