package com.example.dailyshoppinglist;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dailyshoppinglist.Model.Data;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.util.Date;

public class HomeActivity extends AppCompatActivity {
    private Toolbar tolbar;
    private FloatingActionButton float_action_btn;
    private DatabaseReference mdbReference;
    private FirebaseAuth mAuth;
    private Button button_save;
    private RecyclerView recyclerView;
    private  TextView totalSumResult;
    private  String type;
    private String note;
    private int amount;
    private String post_key;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        mAuth = FirebaseAuth.getInstance();
        FirebaseUser mUser = mAuth.getCurrentUser();
        String uId = mUser.getUid();
        mdbReference = FirebaseDatabase.getInstance().getReference().child("shopping list").child(uId);
             mdbReference.keepSynced(true);


             mdbReference.addValueEventListener(new ValueEventListener() {
                 @Override
                 public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                     int totalAmount = 0;
                     for(DataSnapshot snap: dataSnapshot.getChildren()){

                          Data data = snap.getValue(Data.class);
                          totalAmount += data.getAmount();
                          String valueTotal = String.valueOf(totalAmount);
                          totalSumResult.setText(valueTotal + ".00");
                     }

                 }

                 @Override
                 public void onCancelled(@NonNull DatabaseError databaseError) {

                 }
             });
        float_action_btn = findViewById(R.id.fab);
        tolbar = findViewById(R.id.home_toolbar);
        setSupportActionBar(tolbar);
        getSupportActionBar().setTitle("Daily Shpping List");

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        totalSumResult = findViewById(R.id.total_amount);
        recyclerView = findViewById(R.id.recycler_home);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
         layoutManager.setStackFromEnd(true);
         layoutManager.setReverseLayout(true);
         recyclerView.setHasFixedSize(true);
         recyclerView.setLayoutManager(layoutManager);

        float_action_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog();
            }
        });
    }

    private void customDialog(){
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = LayoutInflater.from(HomeActivity.this);
        View view = inflater.inflate(R.layout.input_dialog, null);
        final AlertDialog myDialog = dialog.create();
        myDialog.setView(view);
        myDialog.show();

        final EditText type, amount, description;

        type = view.findViewById(R.id.edt_type);
        amount = view.findViewById(R.id.edt_amount);
        description = view.findViewById(R.id.edt_descript);
        button_save = view.findViewById(R.id.add_button);

        button_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String myType = type.getText().toString().trim();
                String myAmount = amount.getText().toString().trim();
                String myDescription= description.getText().toString().trim();

                int amountt = Integer.parseInt(myAmount);

                if (TextUtils.isEmpty(myType)){
                    type.setError("Required field...");
                    return;
                }
                if (TextUtils.isEmpty(myAmount)){
                    amount.setError("Required field...");
                    return;
                }
                if (TextUtils.isEmpty(myDescription)){
                    description.setError("Required field...");
                    return;
                }

                String id = mdbReference.push().getKey();
                String date = DateFormat.getDateInstance().format(new Date());
                Data data  = new Data(myType,myDescription,id,amountt,date);
                mdbReference.child(id).setValue(data);
                Toast.makeText(HomeActivity.this, "data is added", Toast.LENGTH_LONG).show();
                myDialog.dismiss();


            }
        });
    }

    @Override
    protected   void onStart() {

        super.onStart();
        FirebaseRecyclerAdapter<Data,myViewHolder> adapter = new FirebaseRecyclerAdapter<Data, myViewHolder>
                (
                        Data.class,
                        R.layout.item_data,
                        myViewHolder.class,
                        mdbReference
                ) {
            @Override
            protected void populateViewHolder(myViewHolder viewHolder, final Data data, final int i) {
                viewHolder.setAmount(data.getAmount());
                viewHolder.setDate(data.getDate());
                viewHolder.setNote(data.getDescription());
                viewHolder.setType(data.getType());
                viewHolder.myView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        post_key = getRef(i).getKey();
                        type = data.getType();
                        note = data.getDescription();
                        amount = data.getAmount();
                        updateData();
                    }
                });

            }
        };
        recyclerView.setAdapter(adapter);
    }

    public static class  myViewHolder extends  RecyclerView.ViewHolder{
    View myView;
        public myViewHolder(@NonNull View itemView) {
            super(itemView);
            myView = itemView;
        }

        public void setType(String type){
        TextView myType = myView.findViewById(R.id.type);
        myType.setText(type);
    }
        public void setNote(String note){
            TextView myNote = myView.findViewById(R.id.note);
            myNote.setText(note);
        }
        public void setDate(String date){
            TextView myDate= myView.findViewById(R.id.date);
            myDate.setText(date);
        }
        public void setAmount(int amount){
            TextView myType = myView.findViewById(R.id.amount);
            String stAmount = String.valueOf(amount);
            myType.setText(stAmount);
        }

    }

    public void updateData(){

        AlertDialog.Builder myDialog = new AlertDialog.Builder(HomeActivity.this);
        LayoutInflater inflater = LayoutInflater.from(HomeActivity.this);
        View mview = inflater.inflate(R.layout.update_input_field, null);
        final AlertDialog dialog = myDialog.create();
        dialog.setView(mview);

         final EditText editText_type = mview.findViewById(R.id.edt_type_update);
        final EditText editText_amounut = mview.findViewById(R.id.edt_amount_update);
        final EditText editText_note = mview.findViewById(R.id.edt_descript_update);
        Button update_button = mview.findViewById(R.id.button_update);
        Button delete_button = mview.findViewById(R.id.button_delete);

          editText_type.setText(type);
          editText_type.setSelection(type.length());

          editText_amounut.setText(String.valueOf(amount));
          editText_amounut.setSelection(String.valueOf(amount).length());

          editText_note.setText(note);
          editText_note.setSelection(note.length());

        update_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = editText_type.getText().toString().trim();
                String update_amount = editText_amounut.getText().toString().trim();
                note = editText_note.getText().toString().trim();

                 amount = Integer.parseInt(update_amount);
                String date = DateFormat.getInstance().format(new Date());

                Data data = new Data(type,note,post_key,amount,date);
                mdbReference.child(post_key).setValue(data);

                dialog.dismiss();

            }
        });

        delete_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

       mdbReference.child(post_key).removeValue();
                dialog.dismiss();

            }
        });

        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.log_out:
                mAuth.signOut();
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
