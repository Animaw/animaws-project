package com.example.dailyshoppinglist.saving_and_fun;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.dailyshoppinglist.R;
import com.example.dailyshoppinglist.Recycler_package.Proverb_Adapter;
import com.example.dailyshoppinglist.Recycler_package.Proverb_Model;
import com.example.dailyshoppinglist.Recycler_package.Saving_Adapter;
import com.example.dailyshoppinglist.Recycler_package.Saving_Model;
import com.example.dailyshoppinglist.SqlitePackage.Proverb_Database;
import com.example.dailyshoppinglist.SqlitePackage.Saving_Database;

import java.util.ArrayList;

public class Saving_Activity extends AppCompatActivity {

    private Toolbar tolbar;
    private Saving_Database saving_database;
    private ArrayList<Saving_Model> saving_models;
    private RecyclerView recyclerView;
    private Button button_saving;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saving_);

        tolbar = findViewById(R.id.saving_toolbar);
        setSupportActionBar(tolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("saving concepts");

        saving_database = new Saving_Database(this);
        saving_models = new ArrayList<>();

        recyclerView = findViewById(R.id.saving_recycler_view);
        button_saving = findViewById(R.id.saving_button);

        button_saving.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    saving_models = saving_database.getAllSavings();
                    Saving_Adapter saving_adapter = new Saving_Adapter(saving_models, getApplicationContext());
                    recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    recyclerView.setAdapter(saving_adapter);
                    recyclerView.setHasFixedSize(true);

                }catch (Exception e){
                    Toast.makeText(getApplicationContext(), "getAll proverbs"+e.getMessage(), Toast.LENGTH_LONG).show();

                }
            }
        });
    }
}
