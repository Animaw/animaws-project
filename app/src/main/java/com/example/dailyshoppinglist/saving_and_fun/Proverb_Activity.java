package com.example.dailyshoppinglist.saving_and_fun;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.dailyshoppinglist.R;
import com.example.dailyshoppinglist.Recycler_package.Joke_Data_Adapter;
import com.example.dailyshoppinglist.Recycler_package.Proverb_Adapter;
import com.example.dailyshoppinglist.Recycler_package.Proverb_Model;
import com.example.dailyshoppinglist.SqlitePackage.Proverb_Database;

import java.util.ArrayList;

public class Proverb_Activity extends AppCompatActivity {

    private Toolbar tolbar;
    private Proverb_Database proverbDatabase;
    private ArrayList<Proverb_Model>proverb_models;
    private  RecyclerView recyclerView;
    private Button button_proverb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proverb_);


        tolbar = findViewById(R.id.proverb_toolbar);
        setSupportActionBar(tolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("proverbs");


        //actionBar.setDisplayHomeAsUpEnabled(false);

        proverbDatabase = new Proverb_Database(this);
        proverb_models = new ArrayList<>();

       recyclerView = findViewById(R.id.proverb_recycler_view);
       button_proverb = findViewById(R.id.proverb_button);

       button_proverb.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               try{
                   proverb_models = proverbDatabase.getAllProverbs();
                   Proverb_Adapter proverb_adapter = new Proverb_Adapter(getApplicationContext(), proverb_models);
                   recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                   recyclerView.setAdapter(proverb_adapter);
                   recyclerView.setHasFixedSize(true);

               }catch (Exception e){
                   Toast.makeText(getApplicationContext(), "getAll proverbs"+e.getMessage(), Toast.LENGTH_LONG).show();

               }
           }
       });

    }


//    public void showProverbs(View view){
//
//
//    }
}
