package com.example.dailyshoppinglist.saving_and_fun;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.dailyshoppinglist.R;
import com.example.dailyshoppinglist.Recycler_package.AboutAs_Model;
import com.example.dailyshoppinglist.Recycler_package.AboutUs_Adapter;
import com.example.dailyshoppinglist.Recycler_package.Help_Adapter;
import com.example.dailyshoppinglist.Recycler_package.Help_Model;
import com.example.dailyshoppinglist.SqlitePackage.AboutUs_Database;
import com.example.dailyshoppinglist.SqlitePackage.Help_Database;

import java.util.ArrayList;

public class Help_Activity extends AppCompatActivity {
    private Toolbar tolbar;
    private Help_Database help_database;
    private ArrayList<Help_Model> help_list;
    private RecyclerView recyclerView;
    private Button button_help;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_);

        tolbar = findViewById(R.id.help_toolbar);
        setSupportActionBar(tolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("help");

        help_database = new Help_Database(this);
        help_list = new ArrayList<>();

        recyclerView = findViewById(R.id.help_recycler_view);
        button_help = findViewById(R.id.help_button);
        button_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    help_list = help_database.getHelps();
                    Help_Adapter help_adapter = new Help_Adapter(help_list, getApplicationContext());
                    recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    recyclerView.setAdapter(help_adapter);
                    recyclerView.setHasFixedSize(true);

                }catch (Exception e){
                    Toast.makeText(getApplicationContext(), "getAll help Info"+e.getMessage(), Toast.LENGTH_LONG).show();

                }
            }
        });
    }
}
