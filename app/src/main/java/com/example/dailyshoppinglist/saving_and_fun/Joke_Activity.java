package com.example.dailyshoppinglist.saving_and_fun;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.dailyshoppinglist.R;
import com.example.dailyshoppinglist.Recycler_package.Joke_Adapter;
import com.example.dailyshoppinglist.Recycler_package.Joking_Model;
import com.example.dailyshoppinglist.Recycler_package.Saving_Adapter;
import com.example.dailyshoppinglist.Recycler_package.Saving_Model;
import com.example.dailyshoppinglist.SqlitePackage.Joking_Database;
import com.example.dailyshoppinglist.SqlitePackage.Saving_Database;

import java.util.ArrayList;

public class Joke_Activity extends AppCompatActivity {

    private Toolbar tolbar;
    private Joking_Database joking_database;
    private ArrayList<Joking_Model> joking_models;
    private RecyclerView recyclerView;
    private Button button_joking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joke_);

        tolbar = findViewById(R.id.joking_toolbar);
        setSupportActionBar(tolbar);
        ActionBar actionBar = getSupportActionBar();
         actionBar.setDisplayHomeAsUpEnabled(true);
         actionBar.setTitle("some jokes");

        joking_database = new Joking_Database(this);
        joking_models = new ArrayList<>();

        recyclerView = findViewById(R.id.joking_recycler_view);
        button_joking = findViewById(R.id.joking_button);

        button_joking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
               joking_models = joking_database.getAllJokes();
                Joke_Adapter joke_adapter = new Joke_Adapter(joking_models, getApplicationContext());
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                recyclerView.setAdapter(joke_adapter);
                recyclerView.setHasFixedSize(true);

            }catch (Exception e){
                Toast.makeText(getApplicationContext(), "getAll proverbs"+e.getMessage(), Toast.LENGTH_LONG).show();

            }
            }
        });
    }
}
