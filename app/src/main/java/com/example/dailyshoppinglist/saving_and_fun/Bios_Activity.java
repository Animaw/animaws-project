package com.example.dailyshoppinglist.saving_and_fun;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.dailyshoppinglist.R;
import com.example.dailyshoppinglist.Recycler_package.Data_model;
import com.example.dailyshoppinglist.Recycler_package.Joke_Data_Adapter;
import com.example.dailyshoppinglist.SqlitePackage.My_Joke_Database;

import java.util.ArrayList;

public class Bios_Activity extends AppCompatActivity {

      private Toolbar toolbar;
      private My_Joke_Database myJokeDatabase;
      private ArrayList<Data_model> data_models;
       private RecyclerView recyclerView ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bios_);


        recyclerView = findViewById(R.id.joke_recycler_view);
        toolbar = findViewById(R.id.joke_toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("some well knowen person bio's");

        myJokeDatabase = new My_Joke_Database(this);
        data_models = new ArrayList<>();

    }

    public void showData(View view){

        try{
            data_models = myJokeDatabase.getAllData();
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            Joke_Data_Adapter data_adapter = new Joke_Data_Adapter(data_models);
            recyclerView.setAdapter(data_adapter);
            recyclerView.setHasFixedSize(true);


        }catch (Exception e){
            Toast.makeText(this, "getAll data"+e.getMessage(), Toast.LENGTH_LONG).show();

        }
    }

}
