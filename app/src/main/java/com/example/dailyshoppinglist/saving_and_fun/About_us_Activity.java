package com.example.dailyshoppinglist.saving_and_fun;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.dailyshoppinglist.R;
import com.example.dailyshoppinglist.Recycler_package.AboutAs_Model;
import com.example.dailyshoppinglist.Recycler_package.AboutUs_Adapter;
import com.example.dailyshoppinglist.Recycler_package.Saving_Adapter;
import com.example.dailyshoppinglist.Recycler_package.Saving_Model;
import com.example.dailyshoppinglist.SqlitePackage.AboutUs_Database;
import com.example.dailyshoppinglist.SqlitePackage.Saving_Database;

import java.util.ArrayList;

public class About_us_Activity extends AppCompatActivity {
    private Toolbar tolbar;
    private AboutUs_Database aboutUs_database;
    private ArrayList<AboutAs_Model> aboutAs_modelArrayList;
    private RecyclerView recyclerView;
    private Button button_about_us;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us_);

        tolbar = findViewById(R.id.about_us_toolbar);
        setSupportActionBar(tolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("About Us");

        aboutUs_database = new AboutUs_Database(this);
        aboutAs_modelArrayList = new ArrayList<>();

        recyclerView = findViewById(R.id.about_us_recycler_view);
        button_about_us = findViewById(R.id.about_us_button);

       button_about_us.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               try{
                   aboutAs_modelArrayList = aboutUs_database.getAllMemberInfo();
                   AboutUs_Adapter aboutUs_adapter = new AboutUs_Adapter(aboutAs_modelArrayList,getApplicationContext());
                   recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                   recyclerView.setAdapter(aboutUs_adapter);
                   recyclerView.setHasFixedSize(true);

               }catch (Exception e){
                   Toast.makeText(getApplicationContext(), "getAll member Info"+e.getMessage(), Toast.LENGTH_LONG).show();

               }
           }
       });
    }
}
