package com.example.dailyshoppinglist.Recycler_package;

public class Saving_Model {

   private String saving_concept;

    public Saving_Model(String saving_concept) {
        this.saving_concept = saving_concept;
    }

    public String getSaving_concept() {
        return saving_concept;
    }

    public void setSaving_concept(String saving_concept) {
        this.saving_concept = saving_concept;
    }
}
