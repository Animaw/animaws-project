package com.example.dailyshoppinglist.Recycler_package;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dailyshoppinglist.R;

import java.util.ArrayList;

public class Saving_Adapter extends RecyclerView.Adapter<Saving_Adapter.Saving_VieHolder> {

       private ArrayList<Saving_Model> savingList;
       private Context context;

    public Saving_Adapter(ArrayList<Saving_Model> savingList, Context context) {
        this.savingList = savingList;
        this.context = context;
    }

    @NonNull
    @Override
    public Saving_VieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.saving_row, parent,false);

        return new Saving_VieHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Saving_VieHolder holder, int position) {
      Saving_Model savingModel = savingList.get(position);
      holder.saving_concept.setText(savingModel.getSaving_concept());
    }

    @Override
    public int getItemCount() {
        return savingList.size();
    }

    public static class Saving_VieHolder extends RecyclerView.ViewHolder{
        TextView saving_concept;

        public Saving_VieHolder(@NonNull View itemView) {
            super(itemView);
            saving_concept = itemView.findViewById(R.id.saving_textview);
        }
    }
}
