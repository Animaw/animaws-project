package com.example.dailyshoppinglist.Recycler_package;

public class Proverb_Model {

 private  String proverbs;

    public Proverb_Model(String proverbs) {
        this.proverbs = proverbs;
    }

    public String getProverbs() {
        return proverbs;
    }

    public void setProverbs(String proverbs) {
        this.proverbs = proverbs;
    }
}
