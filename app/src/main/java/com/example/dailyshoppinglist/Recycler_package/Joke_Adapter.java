package com.example.dailyshoppinglist.Recycler_package;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dailyshoppinglist.R;

import java.util.ArrayList;

public class Joke_Adapter extends RecyclerView.Adapter<Joke_Adapter.JokingViewHolder> {

    private ArrayList<Joking_Model>  jokingList;
       private Context context;

    public Joke_Adapter(ArrayList<Joking_Model> jokingList, Context context) {
        this.jokingList = jokingList;
        this.context = context;
    }


    @NonNull
    @Override
    public JokingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.joking_row, parent,false);


        return new JokingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull JokingViewHolder holder, int position) {

          Joking_Model joking_model = jokingList.get(position);
          holder.joking_teTextView.setText(joking_model.getJoking());
    }

    @Override
    public int getItemCount() {
        return jokingList.size();
    }

    public class JokingViewHolder extends RecyclerView.ViewHolder{

        TextView joking_teTextView;
        public JokingViewHolder(@NonNull View itemView) {
            super(itemView);
            joking_teTextView = itemView.findViewById(R.id.joking_textview);
        }
    }
}
