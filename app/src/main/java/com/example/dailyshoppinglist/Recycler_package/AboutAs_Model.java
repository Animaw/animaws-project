package com.example.dailyshoppinglist.Recycler_package;

public class AboutAs_Model {
    private String aboutUs;

    public AboutAs_Model(String aboutUs) {
        this.aboutUs = aboutUs;
    }

    public String getAboutUs() {
        return aboutUs;
    }

    public void setAboutUs(String aboutUs) {
        this.aboutUs = aboutUs;
    }
}
