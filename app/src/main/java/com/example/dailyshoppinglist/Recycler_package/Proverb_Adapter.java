package com.example.dailyshoppinglist.Recycler_package;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dailyshoppinglist.R;

import java.util.ArrayList;

public class Proverb_Adapter extends RecyclerView.Adapter <Proverb_Adapter.Proverb_ViewHolder>{

       ArrayList<Proverb_Model> proverb_list;
       Context context;

    public Proverb_Adapter(Context context,ArrayList<Proverb_Model> proverb_list) {
        this.proverb_list = proverb_list;
        this.context = context;
    }

    @NonNull
    @Override
    public Proverb_ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
           View view = LayoutInflater.from(parent.getContext())
                   .inflate(R.layout.proverbs_row,parent, false);
        return new Proverb_ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Proverb_ViewHolder holder, int position) {
        Proverb_Model proverbModel = proverb_list.get(position);
          holder.proverb_Text.setText(proverbModel.getProverbs());

    }

    @Override
    public int getItemCount() {
        return proverb_list.size();
    }

    public static class Proverb_ViewHolder extends RecyclerView.ViewHolder{

   TextView proverb_Text;
        public Proverb_ViewHolder(@NonNull View itemView) {
            super(itemView);
            proverb_Text = itemView.findViewById(R.id.proverb_textview);
        }
    }
}
