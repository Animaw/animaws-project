package com.example.dailyshoppinglist.Recycler_package;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dailyshoppinglist.R;

import java.util.ArrayList;

public class Help_Adapter extends RecyclerView.Adapter <Help_Adapter.HelpViewHolder>{

    ArrayList<Help_Model> helpModelArrayList;
    Context context;

    public Help_Adapter(ArrayList<Help_Model> helpModelArrayList, Context context) {
        this.helpModelArrayList = helpModelArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public HelpViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.help_row, parent,false);


        return new HelpViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HelpViewHolder holder, int position) {
     Help_Model help_model = helpModelArrayList.get(position);
     holder.help_TextView.setText(help_model.getHelp());
    }

    @Override
    public int getItemCount() {
        return helpModelArrayList.size();
    }

    public static class HelpViewHolder extends RecyclerView.ViewHolder{
     TextView help_TextView;
        public HelpViewHolder(@NonNull View itemView) {
            super(itemView);
            help_TextView = itemView.findViewById(R.id.help_textview);
        }
    }
}
