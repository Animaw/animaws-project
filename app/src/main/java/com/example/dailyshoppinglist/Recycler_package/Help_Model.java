package com.example.dailyshoppinglist.Recycler_package;

public class Help_Model {
    String help;

    public Help_Model(String help) {
        this.help = help;
    }

    public String getHelp() {
        return help;
    }

    public void setHelp(String help) {
        this.help = help;
    }
}
