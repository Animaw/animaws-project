package com.example.dailyshoppinglist.Recycler_package;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dailyshoppinglist.R;

import java.util.ArrayList;

public class AboutUs_Adapter extends RecyclerView.Adapter<AboutUs_Adapter.AboutUsViewHolder> {

        private ArrayList<AboutAs_Model> aboutAs;
        Context context;

    public AboutUs_Adapter(ArrayList<AboutAs_Model> aboutAs, Context context) {
        this.aboutAs = aboutAs;
        this.context = context;
    }

    @NonNull
    @Override
    public AboutUsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.about_us_row,parent, false);
        return new AboutUsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AboutUsViewHolder holder, int position) {
        AboutAs_Model aboutAs_model = aboutAs.get(position);
        holder.about_us_TextView.setText(aboutAs_model.getAboutUs());

    }

    @Override
    public int getItemCount() {
        return aboutAs.size();
    }

    public static class AboutUsViewHolder extends RecyclerView.ViewHolder{
        TextView about_us_TextView;

        public AboutUsViewHolder(@NonNull View itemView) {
            super(itemView);
            about_us_TextView = itemView.findViewById(R.id.about_us_textview);
        }
    }
}
