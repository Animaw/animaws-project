package com.example.dailyshoppinglist.Recycler_package;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dailyshoppinglist.R;

import java.util.List;

public class Joke_Data_Adapter extends RecyclerView.Adapter<Joke_Data_Adapter.JokeViewHolder>{

     private List<Data_model> data_modelList;

    public Joke_Data_Adapter(List<Data_model> data_modelList) {
        this.data_modelList = data_modelList;
    }

    @NonNull
    @Override
    public JokeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row,parent, false);
        return new JokeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull JokeViewHolder holder, int position) {

        Data_model data_model = data_modelList.get(position);
        holder.imageView.setImageBitmap(data_model.getImages());
        holder.textView.setText(data_model.getImageDescription());

    }


    @Override
    public int getItemCount() {
        return data_modelList.size();
    }

    public static class JokeViewHolder extends RecyclerView.ViewHolder{

         ImageView imageView;
         TextView textView;

        public JokeViewHolder(@NonNull View itemView) {

            super(itemView);
            imageView = itemView.findViewById(R.id.joke_imageview);
            textView =itemView.findViewById(R.id.joke_textview);

        }
    }
}
