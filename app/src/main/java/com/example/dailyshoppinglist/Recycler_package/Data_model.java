package com.example.dailyshoppinglist.Recycler_package;

import android.graphics.Bitmap;

public class Data_model {
    String imageDescription;
    Bitmap images;

    public Data_model(String imageDescription, Bitmap images) {
        this.imageDescription = imageDescription;
        this.images = images;
    }

    public String getImageDescription() {
        return imageDescription;
    }

    public void setImageDescription(String imageDescription) {
        this.imageDescription = imageDescription;
    }

    public Bitmap getImages() {
        return images;
    }

    public void setImages(Bitmap images) {
        this.images = images;
    }
}
