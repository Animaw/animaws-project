package com.example.dailyshoppinglist.SqlitePackage;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.example.dailyshoppinglist.Recycler_package.AboutAs_Model;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

public class AboutUs_Database extends SQLiteAssetHelper {
    private static final String DATABASENAME ="aboutUsDatabase.db";
    private static final int DATABASEVERSION =1;
    Context context;
    public AboutUs_Database(Context context) {
        super(context, DATABASENAME, null, DATABASEVERSION);
        this.context = context;
    }

    public ArrayList<AboutAs_Model> getAllMemberInfo(){

        try {
            ArrayList<AboutAs_Model> aboutAs_ArrayList = new ArrayList<>();
            SQLiteDatabase objSqLiteDatabase = getReadableDatabase();

            if (objSqLiteDatabase != null){
                Cursor cursor = objSqLiteDatabase.rawQuery("select * from AboutUs", null);
                if (cursor.getCount() != 0){

                    while (cursor.moveToNext()){
                        String aboutUs = cursor.getString(0);
                        aboutAs_ArrayList.add(new AboutAs_Model(aboutUs));

                    }
                    return aboutAs_ArrayList;
                }
                else {
                    Toast.makeText(context, "No data is retrived...", Toast.LENGTH_LONG).show();
                    return null;
                }

            }else {
                Toast.makeText(context, "Database is null...", Toast.LENGTH_LONG).show();
                return null;
            }

        }catch (Exception e){
            Toast.makeText(context, "get all member Info" + e.getMessage(), Toast.LENGTH_LONG).show();
            return null;
        }
    }
}
