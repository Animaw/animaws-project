package com.example.dailyshoppinglist.SqlitePackage;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.example.dailyshoppinglist.Recycler_package.Joking_Model;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

public class Joking_Database extends SQLiteAssetHelper {
    private static final String DATABASENAME ="JokingDatabase.db";
    private static final int DATABASEVERSION =1;

    Context context;
    public Joking_Database(Context context) {
        super(context, DATABASENAME, null, DATABASEVERSION);
        this.context = context;
    }

    public ArrayList<Joking_Model>  getAllJokes(){
        try{

            ArrayList<Joking_Model> joking_modelArrayList = new ArrayList<>();
            SQLiteDatabase objSqLiteDatabase = getReadableDatabase();

            if (objSqLiteDatabase != null){
                Cursor cursor = objSqLiteDatabase.rawQuery("select * from Joking", null);
                if (cursor.getCount() != 0){

                    while (cursor.moveToNext()){
                        String joking = cursor.getString(0);
                        joking_modelArrayList.add(new Joking_Model(joking));

                    }
                    return joking_modelArrayList;
                }
                else {
                    Toast.makeText(context, "No data is retrived...", Toast.LENGTH_LONG).show();
                    return null;
                }

            }else {
                Toast.makeText(context, "Database is null...", Toast.LENGTH_LONG).show();
                return null;
            }

        }
        catch (Exception e){
            Toast.makeText(context, "getAll jokings"+e.getMessage(), Toast.LENGTH_LONG).show();
            return null;
        }

    }
}
