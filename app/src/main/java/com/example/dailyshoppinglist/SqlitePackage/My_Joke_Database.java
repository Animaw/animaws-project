package com.example.dailyshoppinglist.SqlitePackage;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.Toast;

import com.example.dailyshoppinglist.Recycler_package.Data_model;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

public class My_Joke_Database extends SQLiteAssetHelper {

    private static final  String DATABASE_NAME = "bioDatabase.db";
    private  static final int DATABASE_VERSION = 1;

    Context context;

    public My_Joke_Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    public ArrayList<Data_model> getAllData(){

        try{

            ArrayList<Data_model> dataModelList = new ArrayList<>();
            SQLiteDatabase objSqLiteDatabase = getReadableDatabase();

            if (objSqLiteDatabase != null){
                Cursor cursor = objSqLiteDatabase.rawQuery("select * from Bio", null);
                      if (cursor.getCount() != 0){

                           while (cursor.moveToNext()){
           byte [] imageBytes = cursor.getBlob(0);
           String imagedescription = cursor.getString(1);
           Bitmap bitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);

                dataModelList.add(new Data_model(imagedescription, bitmap));
                           }
                           return dataModelList;
                      }
                      else {
                          Toast.makeText(context, "No data", Toast.LENGTH_LONG).show();
                          return null;
                      }

            }else {
                Toast.makeText(context, "No data", Toast.LENGTH_LONG).show();
                return null;
            }

        }
        catch (Exception e){
            Toast.makeText(context, "getAll data"+e.getMessage(), Toast.LENGTH_LONG).show();
            return null;
        }
    }
}
