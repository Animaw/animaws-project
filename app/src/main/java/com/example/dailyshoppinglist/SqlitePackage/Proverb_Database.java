package com.example.dailyshoppinglist.SqlitePackage;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;
import com.example.dailyshoppinglist.Recycler_package.Proverb_Model;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

public class Proverb_Database extends SQLiteAssetHelper {

    private static final  String DATABASE_NAME = "myproverbs.db";
    private  static final int DATABASE_VERSION = 1;

    Context context;

    public Proverb_Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    public ArrayList<Proverb_Model> getAllProverbs(){

        try{

            ArrayList<Proverb_Model> proverb_modelArrayList = new ArrayList<>();
            SQLiteDatabase objSqLiteDatabase = getReadableDatabase();

            if (objSqLiteDatabase != null){
                Cursor cursor = objSqLiteDatabase.rawQuery("select * from proverbs", null);
                if (cursor.getCount() != 0){

                    while (cursor.moveToNext()){
                        String proverbs = cursor.getString(0);
                        proverb_modelArrayList.add(new Proverb_Model(proverbs));

                    }
                    return proverb_modelArrayList;
                }
                else {
                    Toast.makeText(context, "No data is retrived...", Toast.LENGTH_LONG).show();
                    return null;
                }

            }else {
                Toast.makeText(context, "Database is null...", Toast.LENGTH_LONG).show();
                return null;
            }

        }
        catch (Exception e){
            Toast.makeText(context, "getAll proverbs"+e.getMessage(), Toast.LENGTH_LONG).show();
            return null;
        }
    }
}
