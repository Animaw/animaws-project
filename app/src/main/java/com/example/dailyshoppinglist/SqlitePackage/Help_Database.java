package com.example.dailyshoppinglist.SqlitePackage;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.example.dailyshoppinglist.Recycler_package.Help_Model;
import com.example.dailyshoppinglist.Recycler_package.Joking_Model;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

public class Help_Database extends SQLiteAssetHelper {
    private static final String DATABASENAME ="HelpDatabase.db";
    private static final int DATABASEVERSION =1;
    Context context;
    public Help_Database(Context context) {
        super(context, DATABASENAME, null, DATABASEVERSION);
        this.context = context;
    }

    public ArrayList<Help_Model> getHelps(){
        try{

            ArrayList<Help_Model> help_modelArrayListist = new ArrayList<>();
            SQLiteDatabase objSqLiteDatabase = getReadableDatabase();

            if (objSqLiteDatabase != null){
                Cursor cursor = objSqLiteDatabase.rawQuery("select * from Help", null);
                if (cursor.getCount() != 0){

                    while (cursor.moveToNext()){
                        String help = cursor.getString(0);
                        help_modelArrayListist.add(new Help_Model(help));

                    }
                    return help_modelArrayListist;
                }
                else {
                    Toast.makeText(context, "No data is retrived...", Toast.LENGTH_LONG).show();
                    return null;
                }

            }else {
                Toast.makeText(context, "Database is null...", Toast.LENGTH_LONG).show();
                return null;
            }

        }
        catch (Exception e){
            Toast.makeText(context, "getAll help info:"+e.getMessage(), Toast.LENGTH_LONG).show();
            return null;
        }


    }
}
