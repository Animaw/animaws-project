package com.example.dailyshoppinglist.SqlitePackage;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.example.dailyshoppinglist.Recycler_package.Proverb_Model;
import com.example.dailyshoppinglist.Recycler_package.Saving_Model;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

public class Saving_Database extends SQLiteAssetHelper {
    private static final  String DATABASE_NAME = "savingDatabase.db";
    private  static final int DATABASE_VERSION = 1;

    Context context;
    public Saving_Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    public ArrayList<Saving_Model> getAllSavings(){

        try{

            ArrayList<Saving_Model> saving_modelArrayList = new ArrayList<>();
            SQLiteDatabase objSqLiteDatabase = getReadableDatabase();

            if (objSqLiteDatabase != null){
                Cursor cursor = objSqLiteDatabase.rawQuery("select * from Saving", null);
                if (cursor.getCount() != 0){

                    while (cursor.moveToNext()){
                        String saving = cursor.getString(0);
                        saving_modelArrayList.add(new Saving_Model(saving));

                    }
                    return saving_modelArrayList;
                }
                else {
                    Toast.makeText(context, "No data is retrived...", Toast.LENGTH_LONG).show();
                    return null;
                }

            }else {
                Toast.makeText(context, "Database is null...", Toast.LENGTH_LONG).show();
                return null;
            }

        }
        catch (Exception e){
            Toast.makeText(context, "getAll savings"+e.getMessage(), Toast.LENGTH_LONG).show();
            return null;
        }
    }
}
